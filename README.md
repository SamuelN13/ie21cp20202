# Introdução

## Equipe

O projeto foi desenvolvido pelos alunos de Engenharia de Computação:

|Nome| gitlab user|
|---|---|
| Lucas Mikaese | mikaese |
| Samuel Novakoski | SamuelN13 |
| Jude Mathieu | judematieu |

# Documentação

A documentação do projeto pode ser acessada pelo link:

> [Calculadora Simples](https://samueln13.gitlab.io/ie21cp20202/)

# Links Úteis

* [Kit de Desenvolvimento Arduino](https://www.arduino.cc/)
* [Sintax Markdown](https://docs.gitlab.com/ee/user/markdown.html)
* [Hugo Website](https://gohugo.io/)

