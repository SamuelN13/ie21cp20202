---
title: ""
date: 2021-05-06T16:51:04-03:00
draft: false
---

# Introdução ao tema

Motivado pela necessidade de fazer cálculos matemáticos de maneira mais rápida e eficiente, esse projeto é uma calculadora. 

# Objetivos

* Tornar mais rápido o cálculo de contas com operações básicas.
* Facilitar o cálculo de contas om operações básicas.

# Materiais utilizados

|Materias|Quantidade|
|---|---|
|Arduíno UNO| 1|
|Visor LCD| 1 |
|Teclado 4x4| 1|

# Diagrama Elétrico

![](diagrama.jpeg)


# Código

```cpp
#include <LiquidCrystal.h>  
#include <Keypad.h>  
#define LIN 4 
#define COL 4  

LiquidCrystal lcd(12, 11, 5, 4, 3, 2);  

char teclas[LIN][COL]={{'1','2','3','+'},{'4','5','6','-'},{'7','8','9','*'},{'C','0','=','/'}};   

byte linhas[LIN]={9, 8, 7, 6};  
byte colunas[COL]={A5, A4, A3, A2};  

Keypad teclado = Keypad(makeKeymap(teclas), linhas, colunas, LIN, COL);  

bool valorInicial = false;  
bool valorFinal = false;  
String num1, num2;  
float total;  
char operador;  

void setup() {  
  lcd.begin(16, 2);  
  lcd.setCursor(0,0);  

  lcd.print("Calculadora");  
  delay(2000);  
  lcd.setCursor(0,1);  
  lcd.clear();  
}

void loop() {  
  char tecla = teclado.getKey();  

  if(tecla >= '0' && tecla <= '9'){  
    if(valorInicial != false){  
      num1 = num1 + tecla;  
      lcd.print(tecla);  
    }  
    else {  
      num2 = num2 + tecla;  
      lcd.print(tecla);  
      valorFinal = true;  
    }  
  }  
  else if(tecla == '+' || tecla == '-' || tecla == '*' || tecla == '/'){  
    valorInicial = true;  
    operador = tecla;  
    lcd.print(operador);  
  }
  else if(valorFinal == true && tecla == '='){  
    if(operador == '+')  
      total = num2.toFloat() + num1.toFloat();  
    else if(operador == '-')  
      total = num2.toFloat() - num1.toFloat();  
    else if(operador == '*')  
      total = num2.toFloat() * num1.toFloat();  
    else  
      total = num2.toFloat() / num1.toFloat();  
    
    lcd.setCursor(0, 1);
    lcd.print(total);
  }
  
  else if(tecla == 'C'){
    lcd.clear();
    valorInicial = false;
    valorFinal = false;
    num1 = "";
    num2 = "";
    total = 0.0;
  operador = ' ';
 }
}

```

# Manual 
1. Primeiro digite um número.
2. Escolha a operação a ser realizada.
3. Confirme a operação e veja o resultado.
4. Apague o que está no visor para fazer um novo cálculo. 

# Resultados

{{< youtube zK485Y_Rb5s >}}

# Desafios encontrados

Houveram alguns problemas envolvendo o simulador utilizado, muitas vezes era necessário criar outro projeto porque o simulador parava de funcionar. Outro empeciho foi a lógica usada primariamente que não estava funcionando, precisando reformular todo o código.

# Conclusões

O projeto no final atendeu bem nossas espectativa. No entanto, existem partes nele que podem ser melhoradas. 

O código em si apresenta algumas limitações por conta do próprio hardware, seja em funcionalidades ou na apresentação. 

Algo que pode melhorar o projeto, por exemplo, é o uso de um lcd maior, permitindo uma disposição melhor de informações, ou o uso de um teclado personalizado, com as funcionalidades sendo demonstradas direto nele, ou também um teclado maior, permitindo a criação de mais funcionalidades para a calculadora. 
