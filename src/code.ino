#include <LiquidCrystal.h>
#include <Keypad.h>

#define LIN 4
#define COL 4

LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

char teclas[LIN][COL]={{'1','2','3','+'},{'4','5','6','-'},{'7','8','9','*'},{'C','0','=','/'}};

byte linhas[LIN]={9, 8, 7, 6};
byte colunas[COL]={A5, A4, A3, A2};

Keypad teclado = Keypad(makeKeymap(teclas), linhas, colunas, LIN, COL);

bool valorInicial = false;
bool valorFinal = false;
String num1, num2;
float total;
char operador;

void setup() {
  lcd.begin(16, 2);
  lcd.setCursor(0,0);

  lcd.print("Calculadora");
  delay(2000);
  lcd.setCursor(0,1);
  lcd.clear();
}

void loop() {
  char tecla = teclado.getKey();

  if(tecla >= '0' && tecla <= '9'){
    if(valorInicial != false){
      num1 = num1 + tecla;
      lcd.print(tecla);
    }
    else {
      num2 = num2 + tecla;
      lcd.print(tecla);
      valorFinal = true;
    }
  }
  else if(tecla == '+' || tecla == '-' || tecla == '*' || tecla == '/'){
    valorInicial = true;
    operador = tecla;
    lcd.print(operador);
  }
  else if(valorFinal == true && tecla == '='){
    if(operador == '+')
      total = num2.toFloat() + num1.toFloat();
    else if(operador == '-')
      total = num2.toFloat() - num1.toFloat();
    else if(operador == '*')
      total = num2.toFloat() * num1.toFloat();
    else
      total = num2.toFloat() / num1.toFloat();
    
    lcd.setCursor(0, 1);
    lcd.print(total);
  }
  
  else if(tecla == 'C'){
    lcd.clear();
    valorInicial = false;
    valorFinal = false;
    num1 = "";
    num2 = "";
    total = 0.0;
    operador = ' ';
  }
}